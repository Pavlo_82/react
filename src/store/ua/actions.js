import { MODAL_ADD_TOGGLE, MODAL_REMOVE_TOGGLE } from './types'

const toggleAddModal = (maybeId) => {
    return {
        type: MODAL_ADD_TOGGLE,
        payload: maybeId,
    }
}

const toggleRemoveModal = (maybeId) => {
    return {
        type: MODAL_REMOVE_TOGGLE,
        payload: maybeId,
    }
}

export { toggleAddModal, toggleRemoveModal }