import { MODAL_ADD_TOGGLE, MODAL_REMOVE_TOGGLE } from './types'

const initialState = {
    isAddModalOpen: false,
    isRemoveModalOpen: false,
    currentProductId: null,
}

export const uiReducer = (state = initialState, action) => {
    switch (action.type){
        case MODAL_ADD_TOGGLE:
            return {
                ...state,
                isAddModalOpen: !state.isAddModalOpen,
                currentProductId: action.payload,
            }
        case MODAL_REMOVE_TOGGLE:
            return {
                ...state,
                isRemoveModalOpen: !state.isRemoveModalOpen,
                currentProductId: action.payload,
            }
        default:
            return state
    }
}