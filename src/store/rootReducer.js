import { combineReducers } from 'redux'
import {productsReducer} from "./products/reducer";
import { uiReducer } from "./ua/reducer";

export const rootReducer = combineReducers({
    products: productsReducer,
    ui: uiReducer,
});