import {
    TOGGLE_PRODUCT_TO_CART,
    GET_PRODUCTS_ERROR,
    GET_PRODUCTS_INIT,
    GET_PRODUCTS_SUCCESS,
    TOGGLE_PRODUCT_TO_FAVORITES
} from './types'
import {store} from '../index'

const getProductsError = (error) => {
    return {
        type: GET_PRODUCTS_ERROR,
        payload: error,
    }
}

const getProductsInit = () => {
    return {
        type: GET_PRODUCTS_INIT
    }
}

const getProductsSuccess = (products) => {
    return {
        type: GET_PRODUCTS_SUCCESS,
        payload: products,
    }
}

const checkProductsForCartAndFavorites = (products, cart, favorites) => {
    const updatedProducts = products.map((product) => {
        const productInCart = cart.some(({id}) => (id === product.id))
        const productInFavorites = favorites.some(({id}) => id === product.id)
        return {...product, isInCart: productInCart, isInFavorites: productInFavorites}
    })
    return updatedProducts
}

const updateStorage = (updatedProducts) => {
    updatedProducts.forEach(product => {
        if (product.isInCart === 'true') {
            localStorage.setItem('cart', JSON.stringify(updatedProducts))
        }
    })
}

export const addProductToCart = (id) => {
    const state = store.getState()
    const products = state.products.products
    const cart = JSON.parse(localStorage.getItem('cart'));
    const product = products.find((product) => product.id === id)
    cart.push(product)
    localStorage.setItem('cart', JSON.stringify(cart))
    // products.map(product => {
    //     if (id === product.id) {
    //         product.isInCart = !product.isInCart
    //     }
    //     return product
    // })
    return {
        type: TOGGLE_PRODUCT_TO_CART,
        payload: product
    }
}

export const addProductToFavorites = (id) => {
    const state = store.getState()
    const products = state.products.products
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    const product = products.find((product) => product.id === id)
    favorites.push(product)
    localStorage.setItem('favorites', JSON.stringify(favorites))
    return {
        type: TOGGLE_PRODUCT_TO_FAVORITES,
        payload: product,
    }
}

export const deleteFromFavorites = (id) => {
    const state = store.getState()
    const products = state.products.products
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    const newFavorites = favorites.filter((product,) => {
        return id !== product.id;
    })
    localStorage.setItem('favorites', JSON.stringify(newFavorites))
    const product = products.find((product) => product.id === id)
    return {
        type: TOGGLE_PRODUCT_TO_FAVORITES,
        payload: product
    }
}

export const deleteFromCart = (id) => {
    const state = store.getState()
    const products = state.products.products
    const productToDelete = JSON.parse(localStorage.getItem('cart'))
    const newCart = productToDelete.filter((product) => {
        return id !== product.id
    })
    localStorage.setItem('cart', JSON.stringify(newCart))
    const product = products.find((product) => product.id === id)
    return {
        type: TOGGLE_PRODUCT_TO_CART,
        payload: product
    }
}

export const getProducts = () => async (dispatch) => {
    dispatch(getProductsInit())
    if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
        localStorage.setItem('cart', JSON.stringify([]))
        localStorage.setItem('favorites', JSON.stringify([]))
    }
    try {
        const response = await fetch('./db.json');
        const products = await response.json();
        const localCart = JSON.parse(localStorage.getItem('cart'))
        const localFavorites = JSON.parse(localStorage.getItem('favorites'))
        const updatedProducts = checkProductsForCartAndFavorites(products.list, localCart, localFavorites)
        updateStorage(updatedProducts)
        dispatch(getProductsSuccess(updatedProducts))
    } catch (error) {
        dispatch(getProductsError(error))
    }
}