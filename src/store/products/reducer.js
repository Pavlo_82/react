import { GET_PRODUCTS_ERROR, GET_PRODUCTS_INIT, GET_PRODUCTS_SUCCESS, TOGGLE_PRODUCT_TO_CART, TOGGLE_PRODUCT_TO_FAVORITES, } from './types'

const initialState = {
    products: [],
    isProductsLoading: false,
    error: null,
}

export const productsReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_PRODUCTS_INIT:
        return {
            ...state,
            isProductsLoading: true,
            error: null,
        }
        case GET_PRODUCTS_SUCCESS:
           return {
            ...state,
            products: action.payload,
            isProductsLoading: false,
        }
        case GET_PRODUCTS_ERROR:
            return {
                ...state,
                error: action.error
            }
        case TOGGLE_PRODUCT_TO_CART:
            return {
                ...state,
                products: state.products.map(product => {
                    console.log(action.payload)
                    if(product.id === action.payload.id){
                        console.log(product.isInCart)
                        return {...product, isInCart: !product.isInCart}
                    } else {return product}
                })
        }
        case TOGGLE_PRODUCT_TO_FAVORITES:
            return {
                ...state,
                products: state.products.map(product => {
                    if(product.id === action.payload.id){
                        return { ...product, isInFavorites: !product.isInFavorites}
                    } else {return product}
                })
            }
        default:
            return state
    }
}