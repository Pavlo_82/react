import React from 'react';
import PropTypes from 'prop-types';
import {Product} from "./Product";
import './products.scss';


export const ProductsList = ({
                                 products,
                                 handleModalOpen,
                                 handleAddToFavorites,
                                 handleRemoveFromFavorites,
                             }) => {
    return (
        <div className="container">
            {products.map((product) => (
                <div key={product.id} className="productItem">
                    <Product {...product} handleModalOpen={handleModalOpen} handleAdd={handleAddToFavorites}
                             handleRemove={handleRemoveFromFavorites}/>
                </div>
            ))}
        </div>
    );
}

ProductsList.propTypes = {
    products: PropTypes.array,
    onClick: PropTypes.func,
    handleModalOpen: PropTypes.func,
}