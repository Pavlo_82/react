import React from 'react';
import PropTypes from "prop-types";
import { Button } from "./Button";
import './Style.scss'

export const Modal = (props) => {

    return (
        <>
            {props.isOpen &&
            <div className="modal">
                <div className="modalBackDrop" onClick={props.onCancel}/>
                <div className="modalDialog">
                    <div className="modalContent">
                        <div className="modalHeader">
                            <h3 className="modalTitle">{props.title}</h3>
                            <Button name="x" className="close" onClick={props.onCancel}/>
                        </div>
                        <div className="modal-body">
                            <div className="ModalBody">
                                {props.children}
                                {/*{this.props.id}*/}
                            </div>
                        </div>
                        <div className="ModalFooter">
                            <Button onClick={props.onCancel} className="btn secondaryButton" name='Cancel'/>
                            <Button className='btn primaryButton' name='Submit' onClick={props.onSubmit}/>
                        </div>
                    </div>
                </div>
            </div>
            }
        </>
    )
}

Modal.propTypes = {
    title: PropTypes.string,
    children: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}