import React from 'react';
import './Button.scss'
import PropTypes from "prop-types";

export const Button = (props) => {
    return (
        <button onClick={props.onClick} className={props.className}>{props.name}</button>
    )
}

Button.propTypes = {
    name: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
}