import { IoIosStarOutline} from "react-icons/io"
import PropTypes from 'prop-types';
import React from "react";
import { connect } from 'react-redux'
import {Button} from "./Button";
import { toggleAddModal } from '../store/ua/actions'
import './Product.scss'
import {addProductToFavorites as addToFavorites, deleteFromFavorites as removeFromFavorites} from "../store/products/action";

const mapDispatchToProps = (dispatch) => {
    return {
        toggleAddModal: (maybeId) => dispatch(toggleAddModal(maybeId)),
        addProductToFavorites: (id) => dispatch(addToFavorites(id)),
        deleteFromFavorites: (id) => dispatch(removeFromFavorites(id))
    }}

export const Product = connect(null, mapDispatchToProps)((props) => {
    const {
        name,
        art,
        color,
        image_url,
        price,
        id,
        isInFavorites,
        handleAdd,
        handleRemove,
    } = props

    return (
        <div>
            <IoIosStarOutline onClick={
                // ()=> isInFavorites ? handleRemove(id) : handleAdd(id)
                ()=>isInFavorites ? props.deleteFromFavorites(id) : props.addProductToFavorites(id)
            } className={isInFavorites ? 'starActive' : ''}/>
            <p>Название: {name}</p>
            <p>Артикул: {art}</p>
            <p>Цвет: {color}</p>
            <img src={image_url} className='img'/>
            <p>Цена: {price} грн</p>
            <Button name='В корзину' onClick={()=>{
                props.toggleAddModal(id)
            }}
                    className='btn btnModal'/>
        </div>
    );
})

Product.propTypes = {
    name: PropTypes.string,
    art: PropTypes.number,
    color: PropTypes.string,
    image_url: PropTypes.string,
    price: PropTypes.number,
    onClick: PropTypes.func,
    handleModalOpen: PropTypes.func,
}