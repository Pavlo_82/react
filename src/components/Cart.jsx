import React from "react";
import {IoIosStarOutline} from "react-icons/io"
import {ImCross} from "react-icons/im";
import PropTypes from 'prop-types';
import './Product.scss'
import {toggleRemoveModal} from "../store/ua/actions";
import {connect} from "react-redux";
import {addProductToFavorites as addToFavorites, deleteFromCart as removeFromCart,} from "../store/products/action";

const mapDispatchToProps = (dispatch) => {
    return {
        toggleRemoveModal: (maybeId) => dispatch(toggleRemoveModal(maybeId)),
        addProductToFavorites: (id) => dispatch(addToFavorites(id)),
        deleteFromCart: (id) => dispatch(removeFromCart(id))
    }
}

export const Cart = connect(null, mapDispatchToProps)((props) => {
    const {
        name,
        art,
        color,
        image_url,
        price,
        id,
        isInFavorites,
    } = props

    return (
        <div>
            <ImCross onClick={() => {
                props.toggleRemoveModal(id)
                props.deleteFromCart(id)
            }} className='cross'/>
            <IoIosStarOutline onClick={() =>
                props.addProductToFavorites(id)
                // isInFavorites ? handleRemove(id) : handleAdd(id)
            }
                              className={isInFavorites ? 'starActive' : ''}/>
            <p>Название: {name}</p>
            <p>Артикул: {art}</p>
            <p>Цвет: {color}</p>
            <img src={image_url} className='img'/>
            <p>Цена: {price} грн</p>
        </div>
    );
})

Cart.propTypes = {
    name: PropTypes.string,
    art: PropTypes.number,
    color: PropTypes.string,
    image_url: PropTypes.string,
    price: PropTypes.number,
    onClick: PropTypes.func,
    handleModalOpen: PropTypes.func,
}