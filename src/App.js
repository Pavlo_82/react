import './App.css';
import { connect } from 'react-redux'
import {BrowserRouter as Router, Link,} from "react-router-dom"
import { useEffect } from 'react'
import { getProducts as getProductsAction } from "./store/products/action";
import { Products } from "./components/Products"

function App(props) {
  useEffect(() => {
    props.getProducts()
  }, [])

  return (
      <Router>
        <div className="App">
          <Link to="/">Продукты</Link> ||
          <Link to="cart">Корзина</Link> ||
          <Link to="favorites">Избранное</Link>
          <Products/>
        </div>
      </Router>
  );
}

const mapStateToProps = (store) => {
  return {
    products: store.products.products,
    isLoading: store.products.isProductsLoading,
    error: store.products.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProducts: () => dispatch(getProductsAction()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);